package io.actions

import model.Action
import org.openqa.selenium.remote.RemoteWebDriver

import java.util.concurrent.TimeUnit

class ActionLoad implements IActionLoader {

    @Override
    void loadActions(Action actionModel, RemoteWebDriver driver) {
        driver.get(actionModel.source)
        sleep(TimeUnit.MILLISECONDS.convert(actionModel.delay, TimeUnit.SECONDS))
    }
}
