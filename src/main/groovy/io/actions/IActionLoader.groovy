package io.actions

import model.Action
import org.openqa.selenium.remote.RemoteWebDriver

interface IActionLoader {

    void loadActions(Action actionModel, RemoteWebDriver driver)
}