package io.actions

import com.machinepublishers.jbrowserdriver.UserAgent
import groovy.util.logging.Slf4j
import model.Action
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.remote.RemoteWebDriver

import java.util.concurrent.TimeUnit

@Slf4j
class ActionClick implements IActionLoader {

    @Override
    void loadActions(Action actionModel, RemoteWebDriver driver) {

//        String popupSelector = "/html/body/div[14]/div/i"
        String popupSelector = "/html/body/div[1]/div[2]/div/div"
        WebElement element = driver.findElementByXPath(popupSelector);
        if (element.isDisplayed() && element.isEnabled()) {
            element.click();
        }
        String xPath = actionModel.source
        def foundElement = driver.findElementsByXPath(xPath)
        if (foundElement.isEmpty()) {
            log.error("${xPath} - element not found")
            return
        }
        WebElement foundElementClick = driver.findElementByXPath(xPath)
        new Actions(driver).moveToElement(foundElementClick).click().perform()

        sleep(TimeUnit.MILLISECONDS.convert(actionModel.delay, TimeUnit.SECONDS))
    }
}
