package io.mongodb

import com.mongodb.Block
import com.mongodb.CursorType
import com.mongodb.async.AsyncBatchCursor
import com.mongodb.async.SingleResultCallback
import com.mongodb.async.client.FindIterable
import com.mongodb.client.model.Collation
import org.bson.Document
import org.bson.conversions.Bson
import mapper.ModelMapper

import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

/**
 * Created by chipn@eway.vn on 2/9/17.
 */
class ModelFindIterable<T> {

    private FindIterable<Document> delegate
    private final ModelMapper<T> mapper

    ModelFindIterable(DocumentFindIterable findIterable, ModelMapper<T> mapper) {
        this(findIterable.delegate, mapper)
    }

    ModelFindIterable(FindIterable<Document> delegate, ModelMapper<T> mapper) {
        this.delegate = delegate
        this.mapper = mapper
    }

    ModelFindIterable filter(Bson filter) {
        delegate.filter(filter)
        return this
    }

    ModelFindIterable limit(int limit) {
        delegate.limit(limit)
        return this
    }

    ModelFindIterable skip(int skip) {
        delegate.skip(skip)
        return this
    }

    ModelFindIterable maxTime(long maxTime, TimeUnit timeUnit) {
        delegate.maxTime(maxTime, timeUnit)
        return this
    }

    ModelFindIterable maxAwaitTime(long maxAwaitTime, TimeUnit timeUnit) {
        delegate.maxAwaitTime(maxAwaitTime, timeUnit)
        return this
    }

    ModelFindIterable modifiers(Bson modifiers) {
        delegate.modifiers(modifiers)
        return this
    }

    ModelFindIterable projection(Bson projection) {
        delegate.projection(projection)
        return this
    }

    ModelFindIterable sort(Bson sort) {
        delegate.sort(sort)
        return this
    }

    ModelFindIterable noCursorTimeout(boolean noCursorTimeout) {
        delegate.noCursorTimeout(noCursorTimeout)
        return this
    }

    ModelFindIterable oplogReplay(boolean oplogReplay) {
        delegate.oplogReplay(oplogReplay)
        return this
    }

    ModelFindIterable partial(boolean partial) {
        delegate.partial(partial)
        return this
    }

    ModelFindIterable cursorType(CursorType cursorType) {
        delegate.cursorType(cursorType)
        return this
    }

    CompletableFuture<Document> firstDocument() {
        MongoCompletableFuture<Document> future = new MongoCompletableFuture<>()
        delegate.first(future)
        return future
    }

    CompletableFuture<T> first() {
        return this.firstDocument().thenApply { document -> mapper.toModel(document) }
    }

    CompletableFuture<List<Document>> into() {
        MongoCompletableFuture<List<Document>> future = new MongoCompletableFuture<>()
        delegate.into(new ArrayList<>(), future)
        return future
    }

    CompletableFuture<List<T>> intoModels() {
        return this.into().thenApply { documents -> mapper.toModels(documents) }
    }

    void forEach(Block<Document> block, SingleResultCallback<Void> callback) {
        delegate.forEach(block, callback)
    }

    void forEachModels(Block<T> block, SingleResultCallback<Void> callback) {
        this.forEach({ document -> block.apply(mapper.toModel(document)) }, callback)
    }

    ModelFindIterable batchSize(int batchSize) {
        delegate.batchSize(batchSize)
        return this
    }

    CompletableFuture<ModelBatchCursor> batchCursor() {
        MongoCompletableFuture<AsyncBatchCursor<Document>> future = new MongoCompletableFuture<>()
        delegate.batchCursor(future)
        return future.thenApply { it -> new ModelBatchCursor(it, mapper) }
    }

    ModelFindIterable collation(Collation collation) {
        delegate.collation(collation)
        return this
    }
}