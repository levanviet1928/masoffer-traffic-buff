package io.mongodb.collection

import io.mongodb.ModelCollection
import mapper.CampaignMapper
import model.Campaign
import org.bson.Document

class CampaignCollection extends ModelCollection<Campaign> {

    CampaignCollection(String uri) {
        super(uri, CampaignMapper.class)
    }

    Campaign findCampaignById(String campaignId) {
        Document filter = [
            "campaign_id": campaignId
        ]
        this.findModels(filter).intoModels().join().first()
    }

}
