package worker

import groovy.util.logging.Slf4j
import io.actions.ActionClick
import io.actions.ActionLoad
import io.actions.ActionScroll
import io.actions.IActionLoader
import model.Action
import model.Campaign
import org.apache.commons.lang.StringUtils
import org.apache.commons.lang3.RandomUtils
import org.openqa.selenium.PageLoadStrategy
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.RemoteWebDriver
import org.redisson.api.RedissonClient

@Slf4j
class AbstractDriverWorker implements Runnable {
    List<String> mobileDevices
    List<String> webUsersAgent
    Campaign campaignInfo
    RedissonClient campaignRedisson

    @Override
    void run() {
        int deviceRate = campaignInfo.deviceRate
        List<String> baseUrls = campaignInfo.baseUrls
        Integer bounceRate = campaignInfo.bounceRate
        Integer sizeUrlsList = baseUrls.size()
        while (true) {
            ChromeOptions options = new ChromeOptions()
                .setHeadless(false)
                .setPageLoadStrategy(PageLoadStrategy.NORMAL)
                .addArguments("–-disable-extensions", "--mute-audio")
                .addArguments("--silent")
                .addArguments("--start-maximized")
                .addArguments("--log-level=3")
                .addArguments("--enable-automation")
                .addArguments("--no-sandbox")
                .addArguments("--disable-dev-shm-usage")

            //setup web and mobile
            List<Action> actions
            int randomDevice = RandomUtils.nextInt(1, 100)
            if (randomDevice <= deviceRate) {
                actions = campaignInfo.mobileActions
                String device = mobileDevices.get(RandomUtils.nextInt(0, mobileDevices.size()))
                options.setExperimentalOption("mobileEmulation", ["deviceName": device])
            } else {
                actions = campaignInfo.webActions
                String userAgent = webUsersAgent.get(RandomUtils.nextInt(0, webUsersAgent.size()))
                options.addArguments("--user-agent=${userAgent}")
            }
            RemoteWebDriver browserDriver
            long startTime = System.currentTimeMillis()
            try {
                browserDriver = new ChromeDriver(options)
                int randomAction = RandomUtils.nextInt(1, 100)
                for (action in actions) {
                    IActionLoader actionLoader = null
                    switch (action.actionType) {
                        case "load":
                            Integer urlIndex = RandomUtils.nextInt(0, sizeUrlsList)
                            String source = baseUrls.get(urlIndex)
                            if (randomDevice < deviceRate) {
                                source = StringUtils.replace(source, "MO10", "MO20")
                            }
                            action.setSource(source)
                            actionLoader = new ActionLoad()
                            break
                        case "click":
                            actionLoader = new ActionClick()
                            break
                        case "scroll":
                            actionLoader = new ActionScroll()
                            break
                    }
                    actionLoader.loadActions(action, browserDriver)
                    if (randomAction <= bounceRate) {
                        campaignRedisson.getAtomicLong("${campaignInfo.campaignId}-load").andIncrement
                        break
                    }
                }
                if (randomAction <= bounceRate) {
                    continue
                }
                campaignRedisson.getAtomicLong("${campaignInfo.campaignId}-all-actions").andIncrement
            } catch (error) {
                log.error("Somestime went wrong. Please check again. Time: ${System.currentTimeMillis() - startTime}", error)
            } finally {
                try {
                    browserDriver.quit()
                } catch (exception) {
                    log.error("Quit driver error. Time: ${System.currentTimeMillis() - startTime} ", exception)
                }
            }
            sleep(2000)
        }
    }
}
