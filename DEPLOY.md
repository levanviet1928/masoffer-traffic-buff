# MO-report

## Rancher service:
`
https://rancher-vn.eway.vn/env/1a3402/apps/stacks/1e102/services/1s1186/containers
`

https://rancher.eway.vn
* Environment: VN-VIETTEL
* Stack: mo-web
* Service: ws-masoffer-report

## ElasticSearch mo_conversions
+ Head plugin: http://sv9.mway.vn:9200/_plugin/head/
+ SQL plugin: http://sv9.mway.vn:9200/_plugin/sql/

## ElasticSearch mo_clicks
**SSH tunel:**
+ TCP: ssh -L 9310:125.212.201.137:9300 debug@sv12.mway.vn
+ HTTP: ssh -L 9210:125.212.201.137:9200 debug@sv12.mway.vn

+ Head plugin: http://sv10.mway.vn:9202/_plugin/head/
+ SQL plugin: http://sv10.mway.vn:9202/_plugin/sql/

## MongoDB ecom-meta
+ uri : io.mongodb://oxugud19:hizope68@io.mongodb.masoffer.com:27018/ecom-meta